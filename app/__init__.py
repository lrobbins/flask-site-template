from flask import Flask, render_template
from app.home import home_blueprint

app = Flask(__name__)

app.config['DEBUG'] = True

#OVERRIDE TEMPLATES
app.config['SECURITY_FORGOT_PASSWORD_TEMPLATE'] = 'security/forgot_password.html'
app.config['SECURITY_LOGIN_USER_TEMPLATE'] = 'security/login_user.html'
app.config['SECURITY_REGISTER_USER_TEMPLATE'] = 'security/register_user.html'
app.config['SECURITY_RESET_PASSWORD_TEMPLATE'] = 'security/reset_password.html'
app.config['SECURITY_CHANGE_PASSWORD_TEMPLATE'] = 'security/change_password.html'
app.config['SECURITY_SEND_CONFIRMATION_TEMPLATE'] = 'security/send_confirmation.html'
app.config['SECURITY_SEND_LOGIN_TEMPLATE'] = 'security/send_login.html'

#POST LOGIN VIEW
app.config['SECURITY_POST_LOGIN_VIEW'] = '/account'
app.config['SECURITY_POST_LOGOUT_VIEW'] = '/'

app.register_blueprint(home_blueprint, url_prefix='/')

@app.errorhandler(401)
def unauthorised(e):
    #TODO: Logger warning
    return render_template('401.html'), 401

@app.errorhandler(403)
def forbidden(e):
    #TODO: Logger warning
    return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(e):
    #TODO: Logger warning
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    #TODO: Logger warning
    return render_template('500.html'), 500