from datetime import datetime
from flask import render_template, Blueprint, current_app
from werkzeug.local import LocalProxy

home_blueprint = Blueprint('home', __name__, template_folder='templates')

#_security = LocalProxy(lambda: current_app.extensions['security'])

@home_blueprint.route('/', methods=["GET"])
def home():
    """Renders the home page."""

    #form = _security.login_form

    return render_template(
        'home/index.html',
        title='Home Page',
        year=datetime.now().year,
     #   login_user_form=form
    )

@home_blueprint.route('contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'home/contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@home_blueprint.route('about')
def about():
    """Renders the about page."""
    return render_template(
        'home/about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )
